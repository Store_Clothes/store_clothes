package com.example.clothes.service;

import com.example.clothes.entity.Category;
import com.example.clothes.entity.Clothes;
import com.example.clothes.entity.Login;
import com.example.clothes.repository.ClothesRepo;
import com.example.clothes.security.RequestInterceptor;
import com.example.clothes.util.JwtTokenUtil;

import io.jsonwebtoken.Claims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class ClothesService {
	@Autowired
	private ClothesRepo clothesRepo;
	@Autowired
	private RestTemplate restTemplate;

	public Clothes addClothes(Clothes clothes, String token) {
		// Kiểm tra token có hợp lệ không
		if (!isTokenValid(token)) {
			throw new IllegalArgumentException("Invalid token");
		}

		// Tạo mã productId ngẫu nhiên và đảm bảo không trùng lặp
		String uniqueID = generateUniqueID();
		clothes.setProductId(uniqueID);
		return clothesRepo.save(clothes);
	}

	private boolean isTokenValid(String token) {
		// Gọi service login để lấy danh sách login
		List<Login> logins = getLoginsFromLoginService();

		// Kiểm tra token trong danh sách login
		for (Login login : logins) {
			if (login.getToken().equals(token) && "admin".equals(login.getUserName())) {
				return true;
			}
		}
		return false;
	}

	private List<Login> getLoginsFromLoginService() {
		String loginServiceUrl = "http://localhost:8085/login/all";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// Gọi service login để lấy danh sách login
		Login[] logins = restTemplate.getForObject(loginServiceUrl, Login[].class);
		return Arrays.asList(logins);
	}

	public List<Clothes> getAllClothes() {
		return clothesRepo.findAll();
	}

	public List<Clothes> findClothesByCategory(String category) {
		return clothesRepo.findByCategory(category);
	}

	public List<Category> getAllCategories() {
		String categoryServiceUrl = "http://localhost:8073/category/all";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ResponseEntity<Category[]> responseEntity = restTemplate.getForEntity(categoryServiceUrl, Category[].class);
		Category[] categories = responseEntity.getBody();
		return Arrays.asList(categories);
	}

	// Hàm tạo mã productId ngẫu nhiên và đảm bảo không trùng lặp
	private String generateUniqueID() {
		String prefix = "PD"; // Chữ cái đầu tiên mặc định
		String randomSuffix = generateRandomSuffix();

		// Kết hợp prefix và suffix
		String productId = prefix + randomSuffix;

		// Kiểm tra xem productId đã tồn tại trong cơ sở dữ liệu hay chưa
		while (clothesRepo.existsByProductId(productId)) { // Sử dụng existsByProductId
			randomSuffix = generateRandomSuffix();
			productId = prefix + randomSuffix;
		}

		return productId;
	}

	// Hàm tạo chuỗi ngẫu nhiên cho 4 chữ số
	private String generateRandomSuffix() {
		Random random = new Random();
		int number = random.nextInt(10000);
		return String.format("%04d", number);
	}
}
