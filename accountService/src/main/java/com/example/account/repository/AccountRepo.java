package com.example.account.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.account.entity.Account;


@Repository
public interface AccountRepo extends JpaRepository<Account, String> {
    boolean existsByIdAccount(String idAccount);
    Optional<Account> findByIdAccount(String idAccount);

    // Xóa tài khoản bằng idAccount
    void deleteByIdAccount(String idAccount);
}