package com.example.account.controller;

import com.example.account.entity.Account;
import com.example.account.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService; // Đảm bảo bạn đã định nghĩa service để xử lý logic

    @PostMapping("/api/create-accounts")
    public void createAccount(@RequestBody Account account) {
        accountService.createAccount(account);
    }
    @GetMapping("/api/user")
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }
    @GetMapping("/api/find-account/{idAccount}")
    public ResponseEntity<Account> getAccountById(@RequestHeader("Authorization") String token, @PathVariable String idAccount) {
        Optional<Account> optionalAccount = accountService.getAccountById(idAccount, token);
        if (optionalAccount.isPresent()) {
            return ResponseEntity.ok(optionalAccount.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/api/accounts/{idAccount}")
    public ResponseEntity<Void> deleteAccount(@RequestHeader("Authorization") String token ,@PathVariable String idAccount) {
        accountService.deleteAccount(idAccount, token);
        return ResponseEntity.noContent().build();
    }
}
