package com.example.account.service;

import com.example.account.entity.Account;
import com.example.account.entity.Login;
import com.example.account.repository.AccountRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class AccountService {

	@Autowired
	private AccountRepo accountRepository;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private RestTemplate restTemplate;

	public List<Account> getAllAccounts() {
		String query = "SELECT user_name, password FROM account";
		return jdbcTemplate.query(query, (rs, rowNum) -> {
			Account account = new Account();
			account.setUserName(rs.getString("user_name"));
			account.setPassword(rs.getString("password"));
			return account;
		});
	}

	public void createAccount(Account account) {
		// Tạo idAccount ngẫu nhiên
		String idAccount = generateUniqueID();

		// Set idAccount cho account
		account.setIdAccount(idAccount);

		// Lưu đối tượng Account vào cơ sở dữ liệu
		accountRepository.save(account);
	}

	public Optional<Account> getAccountById(String idAccount, String token) {
		if (!isTokenValid(token)) {
			throw new IllegalArgumentException("Invalid token");
		}
		return accountRepository.findById(idAccount);
	}

	public void deleteAccount(String idAccount, String token) {
		if (!isTokenValid(token)) {
			throw new IllegalArgumentException("Invalid token");
		}
		// Xóa tài khoản từ cơ sở dữ liệu
		accountRepository.deleteById(idAccount);
	}

	// Hàm tạo mã idAccount ngẫu nhiên và đảm bảo không trùng lặp
	private String generateUniqueID() {
		String prefix = "KH"; // Chữ cái đầu tiên mặc định
		String randomSuffix = generateRandomSuffix();

		// Kết hợp prefix và suffix
		String idAccount = prefix + randomSuffix;

		// Kiểm tra xem idAccount đã tồn tại trong cơ sở dữ liệu hay chưa
		while (accountRepository.existsByIdAccount(idAccount)) {
			randomSuffix = generateRandomSuffix();
			idAccount = prefix + randomSuffix;
		}

		return idAccount;
	}

	// Hàm tạo chuỗi ngẫu nhiên cho 4 chữ số
	private String generateRandomSuffix() {
		Random random = new Random();
		int number = random.nextInt(10000);
		return String.format("%04d", number);
	}

	private boolean isTokenValid(String token) {
		// Gọi service login để lấy danh sách login
		List<Login> logins = getLoginsFromLoginService();

		// Kiểm tra token trong danh sách login
		for (Login login : logins) {
			if (login.getToken().equals(token) && "admin".equals(login.getUserName())) {
				return true;
			}
		}
		return false;
	}

	private List<Login> getLoginsFromLoginService() {
		String loginServiceUrl = "http://localhost:8085/login/all";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		// Gọi service login để lấy danh sách login
		Login[] logins = restTemplate.getForObject(loginServiceUrl, Login[].class);
		return Arrays.asList(logins);
	}
}