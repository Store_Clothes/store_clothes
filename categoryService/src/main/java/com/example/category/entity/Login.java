package com.example.category.entity;



public class Login {
	
	private int idLog;
	
	private String userName;

	private String password;

	private String token;

	public Login(int idLog, String userName, String password, String token) {
		super();
		this.idLog = idLog;
		this.userName = userName;
		this.password = password;
		this.token = token;
	}

	public int getIdLog() {
		return idLog;
	}

	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Login() {
		super();
	}

	@Override
	public String toString() {
		return "Login [idLog=" + idLog + ", userName=" + userName + ", password=" + password + ", token=" + token + "]";
	}

}
