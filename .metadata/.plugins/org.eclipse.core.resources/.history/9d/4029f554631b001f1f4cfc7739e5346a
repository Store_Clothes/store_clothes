package com.example.clothes.controller;

import com.example.clothes.entity.Category;
import com.example.clothes.entity.Clothes;
import com.example.clothes.service.ClothesService;
import com.example.login.util.JwtTokenUtil;

import io.jsonwebtoken.Claims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clothes")
public class ClothesController {

    @Autowired
    private ClothesService clothesService;
    @Autowired
    private ClothesRepo clothesRepo;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @PostMapping("/add")
    public ResponseEntity<Clothes> addClothes(@RequestHeader("Authorization") String token, @RequestBody Clothes clothes) {
    	 try {
             Claims claims = jwtTokenUtil.getClaimsFromToken(token);
             System.out.println("Token valid for user: " + claims.getSubject());

             // Tạo mã productId ngẫu nhiên và đảm bảo không trùng lặp
             String uniqueID = generateUniqueID();
             clothes.setProductId(uniqueID);
             return clothesRepo.save(clothes);
         } catch (Exception e) {
             throw new IllegalArgumentException("Invalid token", e);
         }
    }

    @GetMapping("/all")
    public ResponseEntity<List<Clothes>> getAllClothes() {
        List<Clothes> clothesList = clothesService.getAllClothes();
        return ResponseEntity.ok(clothesList);
    }

    @PostMapping("/category")
    public ResponseEntity<List<Clothes>> getClothesByCategory(@RequestBody Category category) {
        List<Clothes> clothesList = clothesService.getClothesByCategory(category.getName());
        return ResponseEntity.ok(clothesList);
    }
}
