package com.example.account.controller;

import com.example.account.entity.Account;
import com.example.account.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService; // Đảm bảo bạn đã định nghĩa service để xử lý logic

    @PostMapping("/api/accounts")
    public void createAccount(@RequestBody Account account) {
        accountService.createAccount(account);
    }
    @GetMapping("/api/user")
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }
    @GetMapping("/api/accounts/{idAccount}")
    public ResponseEntity<Account> getAccountById(@PathVariable String idAccount) {
        Optional<Account> optionalAccount = accountService.getAccountById(idAccount);
        if (optionalAccount.isPresent()) {
            return ResponseEntity.ok(optionalAccount.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/api/accounts/{idAccount}")
    public ResponseEntity<Void> deleteAccount(@PathVariable String idAccount) {
        accountService.deleteAccount(idAccount);
        return ResponseEntity.noContent().build();
    }
}
