package com.example.signup.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RetryConfig {

    @Bean
    public RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();
        // Cấu hình số lần retry, khoảng thời gian giữa các retry, và các cài đặt khác nếu cần.
        return retryTemplate;
    }
}
