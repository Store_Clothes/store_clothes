package com.example.signup.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;

import com.example.signup.entity.Account;

public class JwtTokenUtil {

	 private static final long JWT_EXPIRATION_MS = 3600000; // Token expiration time: 1 hour

	    public static String createJwtToken(Account account) {
	        // Generate a secure secret key
	        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

	        // Create JWT token with user authentication information
	        return Jwts.builder()
	                .setSubject(account.getUserName()) // Set the subject of the token to the user's username
	                .setIssuedAt(new Date()) // Set the token's issue time to the current time
	                .setExpiration(new Date(System.currentTimeMillis() + JWT_EXPIRATION_MS)) // Set the token's expiration time
	                .signWith(key) // Sign the token with the secure secret key
	                .compact(); // Generate the JWT token string
	    }
}
