package com.example.signup.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

public class Account {

	private String idAccount;

	private String accountName;

	private boolean gender;

	private String address;

	private String userName;

	private String password;

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Account(String idAccount, String accountName, boolean gender, String address, String userName,
			String password) {
		super();
		this.idAccount = idAccount;
		this.accountName = accountName;
		this.gender = gender;
		this.address = address;
		this.userName = userName;
		this.password = password;
	}

	public Account() {
		super();
	}

	@Override
	public String toString() {
		return "Account [idAccount=" + idAccount + ", accountName=" + accountName + ", gender=" + gender + ", address="
				+ address + ", userName=" + userName + ", password=" + password + "]";
	}

}
