package com.example.signup.entity;

public class SignUpResponse {

	private String jwtToken;
	private Account account;

	public SignUpResponse(String jwtToken, Account account) {
		super();
		this.jwtToken = jwtToken;
		this.account = account;
	}

	public SignUpResponse() {
		super();
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "SignUpReponse [jwtToken=" + jwtToken + ", account=" + account + "]";
	}

}
