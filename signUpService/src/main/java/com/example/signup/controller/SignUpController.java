package com.example.signup.controller;

import com.example.signup.entity.Account;
import com.example.signup.service.SignUpService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sign")
public class SignUpController {
	private SignUpService service;

	@Autowired
	public SignUpController(SignUpService service) {
		this.service = service;
	}

	@PostMapping("/api/accounts")
	public void createAccount(@RequestBody Account account) {

		service.signUp(account);

	}
}
