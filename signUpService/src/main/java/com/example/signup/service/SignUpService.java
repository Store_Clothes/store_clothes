package com.example.signup.service;

import com.example.signup.entity.Account;
import com.example.signup.util.JwtTokenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class SignUpService {

	@Autowired
	private RestTemplate restTemplate;

	public ResponseEntity<String> signUp(Account account) {
	    // Generate JWT token
	    String jwtToken = JwtTokenUtil.createJwtToken(account);

	    // Add JWT token to the header of the request
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.set("Authorization", "Bearer " + jwtToken);
	    HttpEntity<Account> requestEntity = new HttpEntity<>(account, headers);

	    // Send POST request to the Account service with JWT token in the header
	    String accountServiceUrl = "http://localhost:8083/account/api/create-accounts";
	    ResponseEntity<Void> responseEntity = restTemplate.exchange(accountServiceUrl, HttpMethod.POST, requestEntity, Void.class);

	    // Check if the request was successful
	    if (responseEntity.getStatusCode().is2xxSuccessful()) {
	        // Print JWT token to console
	        System.out.println("Generated JWT Token: " + jwtToken);
	        
	        // Return the JWT token along with a success message
	        return ResponseEntity.ok("Successfully signed up. JWT Token: " + jwtToken);
	    } else {
	        // Handle the case where the request was not successful
	        return ResponseEntity.status(responseEntity.getStatusCode()).body("Failed to sign up");
	    }
	}

}
