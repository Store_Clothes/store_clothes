package com.example.student.controller;

import com.example.student.entity.Student;
import com.example.student.entity.StudentAndMentor;
import com.example.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    // Endpoint để thêm một sinh viên mới
    @PostMapping("/add")
    public Student addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    // Endpoint để hiển thị danh sách tất cả sinh viên
    @GetMapping("/all")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }
    
    @GetMapping("/with-mentor-info")
    public List<StudentAndMentor> getStudentsWithMentorInfo() {
        return studentService.getStudentsWithMentorInfo();
    }
}
