package com.example.student.service;

import com.example.student.entity.Mentor;
import com.example.student.entity.Student;
import com.example.student.entity.StudentAndMentor;
import com.example.student.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RetryTemplate retryTemplate;
    // Thêm một sinh viên mới
    public Student addStudent(Student student) {
        return studentRepo.save(student);
    }

    // Lấy danh sách tất cả sinh viên
    public List<Student> getAllStudents() {
        return studentRepo.findAll();
    }
    
    public List<StudentAndMentor> getStudentsWithMentorInfo() {
        // Gửi yêu cầu HTTP đến endpoint "/mentors/all" của service mentor
        String mentorsUrl = "http://localhost:8083/mentors/all";
        Mentor[] mentors = retryTemplate.execute(context -> restTemplate.getForObject(mentorsUrl, Mentor[].class));

        // Lấy danh sách sinh viên từ cơ sở dữ liệu
        List<Student> students = studentRepo.findAll();

        // Tạo danh sách mới để lưu thông tin của mỗi sinh viên kèm theo thông tin mentor
        List<StudentAndMentor> studentsWithMentorInfo = new ArrayList<>();

        // Kết hợp thông tin mentor vào danh sách sinh viên
        for (Student student : students) {
        	StudentAndMentor studentWithMentor = new StudentAndMentor();
            studentWithMentor.setStudent(student);

            for (Mentor mentor : mentors) {
                if (Integer.valueOf(student.getMentorId()).equals(mentor.getMentorId())) {
                    studentWithMentor.setMentor(mentor);
                    break;
                }
            }

            studentsWithMentorInfo.add(studentWithMentor);
        }

        return studentsWithMentorInfo;
    }
}
