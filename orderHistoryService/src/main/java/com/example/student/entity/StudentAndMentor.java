package com.example.student.entity;

public class StudentAndMentor {
	private Student student;
	private Mentor mentor;

	public StudentAndMentor(Student student, Mentor mentor) {
		super();
		this.student = student;
		this.mentor = mentor;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Mentor getMentor() {
		return mentor;
	}

	public void setMentor(Mentor mentor) {
		this.mentor = mentor;
	}

	public StudentAndMentor() {
		super();
	}

	@Override
	public String toString() {
		return "StudentAndMentor [student=" + student + ", mentor=" + mentor + "]";
	}

}
