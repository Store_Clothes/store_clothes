package com.example.student.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "student")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int studenId;

	@Column(name = "name")
	private String name;

	@Column(name = "age")
	private int age;

	@Column(name = "mentorId")
	private int mentorId;

	public int getStudenId() {
		return studenId;
	}

	public void setStudenId(int studenId) {
		this.studenId = studenId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getMentorId() {
		return mentorId;
	}

	public void setMentorId(int mentorId) {
		this.mentorId = mentorId;
	}

	public Student(int studenId, String name, int age, int mentorId) {
		super();
		this.studenId = studenId;
		this.name = name;
		this.age = age;
		this.mentorId = mentorId;
	}

	public Student() {
		super();
	}

	@Override
	public String toString() {
		return "Student [studenId=" + studenId + ", name=" + name + ", age=" + age + ", mentorId=" + mentorId + "]";
	}

}
