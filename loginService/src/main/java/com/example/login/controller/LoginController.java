package com.example.login.controller;

import com.example.login.entity.Login;
import com.example.login.service.LoginService;
import com.example.login.entity.Login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/login")
public class LoginController {

	private final LoginService loginService;

	public LoginController(LoginService loginService) {
		this.loginService = loginService;
	}

	@PostMapping("/login")
	public String login(@RequestBody Login login) {
		// Gọi phương thức login từ service và trả về JWT token (nếu đăng nhập thành
		// công)
		return loginService.login(login);
	}

	@GetMapping("/all")
	public List<Login> getAllLogins() {
		// Gọi phương thức để lấy danh sách toàn bộ các đối tượng Login từ service
		return loginService.getAllLogins();
	}
}
