package com.example.login.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.example.login.entity.Account;
import com.example.login.entity.Login;
import com.example.login.repository.LoginRepo;
import com.example.login.util.JwtTokenUtil;

import jakarta.transaction.Transactional;

@Service
public class LoginService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private LoginRepo loginRepository;

	@Transactional
	public String login(Login login) {
		// Gửi yêu cầu HTTP GET để lấy danh sách tất cả các tài khoản từ service Account
		String accountServiceUrl = "http://localhost:8083/account/api/user";
		Account[] accounts = restTemplate.getForObject(accountServiceUrl, Account[].class);

		// Tiếp tục xử lý đăng nhập dựa trên danh sách tài khoản đã nhận được
		for (Account account : accounts) {
			if (account.getUserName().equals(login.getUserName())
					&& account.getPassword().equals(login.getPassword())) {
				if ("admin".equals(login.getUserName()) && "admin".equals(login.getPassword())) {
					// Đăng nhập thành công với quyền admin: Tạo và trả về JWT token đặc biệt
					System.out.println("Admin login successful: " + account.getUserName());
					String token = JwtTokenUtil.createJwtToken("admin");
					login.setToken(token); // Lưu token vào thuộc tính token của đối tượng Login
					loginRepository.save(login); // Lưu thông tin đăng nhập vào cơ sở dữ liệu
					return token;
				} else {
					// Đăng nhập thành công với user bình thường: Tạo và trả về JWT token bình
					// thường
					System.out.println("User login successful: " + account.getUserName());
					String token = JwtTokenUtil.createJwtToken(account.getUserName());
					login.setToken(token); // Lưu token vào thuộc tính token của đối tượng Login
					loginRepository.save(login); // Lưu thông tin đăng nhập vào cơ sở dữ liệu
					return token;
				}
			}
		}

		// Trả về "wrong" nếu không tìm thấy tài khoản hoặc thông tin đăng nhập không
		// chính xác
		System.out.println("Login failed for user: " + login.getUserName());
		return "wrong";
	}

	public List<Login> getAllLogins() {
		return loginRepository.findAll();
	}
}